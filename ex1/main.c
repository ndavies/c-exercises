/*
    Welcome to your first C Program

    NEED TO INSTALL:
    To compile this, we are going to use GNU GCC
    - OSX users have this installed already
    - Windows users need to install and add to PATH
    - Linux users should easily be able to install using apt-get (Ubuntu) or similar

    TO RUN:
    Open up terminal, CMD or similar.
    Find this file in the terminal
    - Linux/OSX use 'cd' to change directory and 'ls' to list files
    - Windows use 'cd' to change director and 'dir' to list files
    
    COMPILE:
    Once at the file, run this line:
    gcc -ansi -W -pedantic -o main main.c

    This uses the GCC compiler to compile/link our main.c file.
    -ansi flag means we compile with the ANSI standard
    -W flag means spit out warnings
    -pedantic flag means be picky about our code 
    -o <file> flag means output to this file (aka the exe)
    main.c is our code file

    RUN:
    If you get no warnings, run the code with ./main or main.exe or ./main.exe
    depending on the os
*/

/* printf lives in this file*/
#include <stdio.h> 

int main(int argc, char* argv[])
{
    /* variables have to be delcared first in ANSI standard*/
    int i;
    char* text;

    /* this is a C for loop, it is printing the arguments */
    for (i = 0; i < argc; i++) {
       printf("%s\n", argv[i]); 
    }

    /* trying running "./main arg1 1 lol" */

    /* this is a print format, \n is a new line */
    printf("Hello world!\n");

    /* lets use that char* text, which is basically a string */
    text = "Why did the chicken cross the road?";

    /* to print it, we use %s, similar to Java */
    printf("%s\n", text);

    /* you can pass it in as many times as you'd like */
    printf("%s, \n%s Who!? \n%s Wha!?\n", text, text, text);

    /* 
        Exercise 1:
        For the following types, 
        create one, at the top,
        and print them out using printf,
        the flags are called "Format parameters" and can be found on Google :)
        
        + char (it will look weird, unless you use ascii tables)
        + short
        + int
        + long
        + long long
        + float
        + double
    */

    /* e.g. %f is for float, printf("%f", myFloat); */

    /* oh, one more thing, C don't care about whitespace */
    printf(
        "This is perfectly valid as it really don\'t care\n"
    ); printf("Please keep your code neat!\n");

    /* we return */
    return 0;
}

/* 
    Bonus 
    ------------
    Find out how to create and print the unsigned version of:
    + char
    + short
    + int
    + long
    + long long

    Super Bonus
    ------------
    Find out how to print a float with 4 decimals after the point:
    i.e. 1.2341234 becomes 1.2341
*/
