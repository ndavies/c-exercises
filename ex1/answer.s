.global main

.data
    .arg: .string "%s\n"
    .helloWorld: .string "Hello World!\n"
    .singleText: .string "Why did the chicken cross the road?"
    .multiText: .string "%s, \n%s Who!?\n%s Wha!?\n"
    .messy_1: .string "This is perfectly valid as it really don\'t care\n"
    .messy_2: .string "Please keep your code neat!\n"

    .char: .string "char = %c\n"
    .short: .string "short = %d\n"
    .int: .string "int = %d\n"
    .long: .string "long = %ld\n"
    .hex: .string "hex = 0x%lX\n"

    .float: .string "float = %f\n"
    .double: .string "double = %g\n"

    .testfloat: .float 1.5
    .testdouble: .double 0.1234

.text

main:
    # save the registers!
    push %r12
    push %r13
    push %r14

    # keep my shit
    xor %r12, %r12
    mov %rdi, %r13
    mov %rsi, %r14

printArg:
    cmp %r12, %r13
    je printEnd

    leaq .arg, %rdi
    mov (%r14, %r12, 8), %rsi

    xor %rax, %rax
    call printf

    inc %r12
    jmp printArg
printEnd:

    leaq .helloWorld, %rdi
    xor %rax, %rax
    call printf

    leaq .arg, %rdi
    leaq .singleText, %rsi
    xor %rax, %rax
    call printf

    leaq .multiText, %rdi
    leaq .singleText, %rsi
    leaq .singleText, %rdx
    leaq .singleText, %rcx
    leaq .singleText, %r8
    xor %rax, %rax
    call printf

    mov $1384596537706222391, %r12

    leaq .char, %rdi
    mov %r12, %rsi
    xor %rax, %rax
    call printf

    leaq .short, %rdi
    xor %rsi, %rsi
    movw %r12w, %si
    xor %rax, %rax
    call printf

    leaq .int, %rdi
    xor %rsi, %rsi
    movl %r12d, %esi
    xor %rax, %rax
    call printf

    leaq .long, %rdi
    mov %r12, %rsi
    xor %rax, %rax
    call printf

    leaq .hex, %rdi
    mov %r12, %rsi
    xor %rax, %rax
    call printf

    # Print float
    leaq .float, %rdi
    movq .testfloat, %xmm0
    cvtss2sd %xmm0, %xmm0
    mov $1, %rax
    call printf

    leaq .double, %rdi
    movq .testdouble, %xmm0
    mov $1, %rax
    call printf

    pop %r14
    pop %r13
    pop %r12

    xor %eax, %eax
    ret

